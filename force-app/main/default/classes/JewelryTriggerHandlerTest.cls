/*
@ Class name        :   JewelryTriggerHandlerTest.cls
@ Created by        :   Rajasekhar Kallepalli
@ Created on        :   03-01-2022
@ Token/Jira Ticket :   SLIV - 70
@ Description       :   Create a JewelryTriggerHandlerTest Class which can be used to handle the unit Tests for Jewelry app. 
*/

@isTest private class JewelryTriggerHandlerTest {
    /*
    @ method name       :   jewelryTest
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To test whether the Total_Price__c value is the product of Unit_Price__c and Quantity__c. 
    */
    @isTest public static void jewelryTest() {
        Integer count = 0;
        List<Jewelry__c> lstJewel = new List<Jewelry__c>();
        Map<Id, Jewelry__c> mapJewelOldUpdate = new Map<Id, Jewelry__c>();
        Map<Id, Jewelry__c> mapJewelNewUpdate = new Map<Id, Jewelry__c>();

        for(Integer i = 0; i < 200; i++)
            lstJewel.add(new Jewelry__c(Name = 'TestJewel' + i, Unit_Price__c = 50, Quantity__c = 10));
        insert lstJewel;

        for(Jewelry__c objJew: lstJewel){
            if(count < 200){
                mapJewelOldUpdate.put(objJew.Id, objJew);
                mapJewelNewUpdate.put(objJew.Id, objJew);
            }
            count += 3;
        }

        for(Jewelry__c objNewJew: mapJewelNewUpdate.values()){
            objNewJew.Name = 'TestJewelNew';
            objNewJew.Unit_Price__c = 60;
            objNewJew.Quantity__c = 15;
        }
        update mapJewelNewUpdate.values();

        Jewelry__c objJewelToTest = [SELECT Total_Price__c, Unit_Price__c FROM Jewelry__c WHERE Unit_Price__c = 60 LIMIT 1];
        System.assertEquals(900, objJewelToTest.Total_Price__c);
    }
}