/*
@ Class name        :   JewelryTriggerHandler.cls
@ Created by        :   Rajasekhar Kallepalli
@ Created on        :   03-01-2022
@ Token/Jira Ticket :   SLIV - 70
@ Description       :   Create a JewelryTriggerHandler Class which can be used to handle the business logic for Jewelry app. 
*/

public with sharing class JewelryTriggerHandler {

    public static void onBeforeInsert(List<Jewelry__c> lstNewJewelries){
		JewelryTriggerHandler.totalPriceCalculatorInsert(lstNewJewelries);
	}

    public static void onBeforeUpdate(Map<Id, Jewelry__c> mapNewJewelries, Map<Id, Jewelry__c> mapOldJewelries){
        JewelryTriggerHandler.totalPriceCalculatorUpdate(mapNewJewelries, mapOldJewelries);
	}
    
    /*
    @ method name       :   totalPriceCalculatorInsert
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To insert the Total_Price__c value as the product of Unit_Price__c and Quantity__c. 
    */

    public static void totalPriceCalculatorInsert(List<Jewelry__c> lstNewJewelries){
        for(Jewelry__c objJew : lstNewJewelries)
            if(objJew.Unit_Price__c != null && objJew.Quantity__c != null)
                objJew.Total_Price__c = objJew.Unit_Price__c * objJew.Quantity__c;
    }
    
    /*
    @ method name       :   totalPriceCalculatorUpdate
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To update the Total_Price__c value as the product of Unit_Price__c and Quantity__c. 
    */

    public static void totalPriceCalculatorUpdate(Map<Id, Jewelry__c> mapNewJewelries, Map<Id, Jewelry__c> mapOldJewelries){
        for(Id objOldId: mapOldJewelries.keySet())
            if(mapNewJewelries.get(objOldId).Quantity__c != mapOldJewelries.get(objOldId).Quantity__c || 
            mapNewJewelries.get(objOldId).Unit_Price__c != mapOldJewelries.get(objOldId).Unit_Price__c)
                mapNewJewelries.get(objOldId).Total_Price__c = mapNewJewelries.get(objOldId).Unit_Price__c * mapNewJewelries.get(objOldId).Quantity__c;
    }
}