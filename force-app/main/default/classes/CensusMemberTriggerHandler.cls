/*
@ Class name        :   CensusMemberTriggerHandler.cls
@ Created by        :   Rajasekhar Kallepalli
@ Created on        :   03-01-2022
@ Token/Jira Ticket :   SLIV - 70
@ Description       :   Create a CensusMemberTriggerHandler Class which can be used to handle the business logic for insurance app. 
*/

public with sharing class CensusMemberTriggerHandler {

    public static void onAfterInsert(List<Census_Member__c> lstNewCensusMembers){
		CensusMemberTriggerHandler.primaryMemberInsert(lstNewCensusMembers);
        CensusMemberTriggerHandler.censusMembersInsert(lstNewCensusMembers);
	}

    public static void onAfterUpdate(Map<Id, Census_Member__c> mapNewCensusMembers, Map<Id, Census_Member__c> mapOldCensusMembers){
        CensusMemberTriggerHandler.primaryMemberUpdate(mapNewCensusMembers, mapOldCensusMembers);
        CensusMemberTriggerHandler.censusMembersUpdate(mapNewCensusMembers, mapOldCensusMembers);
	}

    public static void onBeforeInsert(List<Census_Member__c> lstNewCensusMembers){
		CensusMemberTriggerHandler.effectiveTermDates(lstNewCensusMembers);
        CensusMemberTriggerHandler.singlePrimaryMemberInsert(lstNewCensusMembers);
	}

    public static void onBeforeUpdate(Map<Id, Census_Member__c> mapNewCensusMembers, Map<Id, Census_Member__c> mapOldCensusMembers){
        CensusMemberTriggerHandler.effectiveTermDates(mapNewCensusMembers.values());
        CensusMemberTriggerHandler.singlePrimaryMemberUpdate(mapNewCensusMembers, mapOldCensusMembers);
	}

    /*
    @ method name       :   primaryMemberInsert
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To update the Primary_Member__c field of the Census__c with the name of the related Census_Member__c when created. 
    */

    //**********************************************************Task #5 insert**********************************************************
    public static void primaryMemberInsert(List<Census_Member__c> lstNewCensusMembers){
        Set<Id> setCenId = new Set<Id>();
        for(Census_Member__c objCenMem: lstNewCensusMembers)
            setCenId.add(objCenMem.vlocity_ins_CensusId__c);
        Map<Id, Census__c> mapCenIdToCensus = new Map<Id, Census__c>([SELECT Id, Primary_Member__c FROM Census__c WHERE Id IN :setCenId]);
        for(Census_Member__c objCM: lstNewCensusMembers)
            if(objCM.vlocity_ins_IsPrimaryMember__c == TRUE && mapCenIdToCensus.get(objCM.vlocity_ins_CensusId__c).Primary_Member__c == NULL)
                mapCenIdToCensus.put(objCM.vlocity_ins_CensusId__c, new Census__c(Id = objCM.vlocity_ins_CensusId__c, Primary_Member__c = objCM.Name));
        if(!mapCenIdToCensus.values().isEmpty())
            update mapCenIdToCensus.values();
    }

    /*
    @ method name       :   primaryMemberInsert
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To update the Primary_Member__c field of the Census__c with the name of the related Census_Member__c when updated. 
    */

    //************************************************************Task #5 update******************************************************
    public static void primaryMemberUpdate(Map<Id, Census_Member__c> mapNewCensusMembers, Map<Id, Census_Member__c> mapOldCensusMembers){
        Set<Id> setCenId = new Set<Id>();
        for(Census_Member__c objCenMem: mapNewCensusMembers.values())
            setCenId.add(objCenMem.vlocity_ins_CensusId__c);

        Map<Id, Census__c> mapCenIdToCensus = new Map<Id, Census__c>([SELECT Id, Primary_Member__c FROM Census__c WHERE Id IN :setCenId]);
        List<Census__c> lstCensusRemove = new List<Census__c>();
        Id idCenMemToCensus;
        Boolean varNewPrimaryCheckbox;

        for(Id objCMId: mapNewCensusMembers.keySet()){
            //variables to make code more readable.
            varNewPrimaryCheckbox = mapNewCensusMembers.get(objCMId).vlocity_ins_IsPrimaryMember__c;
            idCenMemToCensus = mapNewCensusMembers.get(objCMId).vlocity_ins_CensusId__c;
            //update the primary member when isPrimary is set to true.
            if(varNewPrimaryCheckbox != mapOldCensusMembers.get(objCMId).vlocity_ins_IsPrimaryMember__c
            && varNewPrimaryCheckbox == TRUE
            && mapCenIdToCensus.get(idCenMemToCensus).Primary_Member__c == NULL)
                mapCenIdToCensus.put(idCenMemToCensus, new Census__c(Id = idCenMemToCensus, Primary_Member__c = mapNewCensusMembers.get(objCMId).Name));
        }

        if(!mapCenIdToCensus.values().isEmpty())
            update mapCenIdToCensus.values();
            
        //Remove the primary member when isPrimary is set to false.
        for(Id objCMId: mapNewCensusMembers.keySet())
            if(mapNewCensusMembers.get(objCMId).vlocity_ins_IsPrimaryMember__c != mapOldCensusMembers.get(objCMId).vlocity_ins_IsPrimaryMember__c
            && mapNewCensusMembers.get(objCMId).vlocity_ins_IsPrimaryMember__c == FALSE)
                lstCensusRemove.add(new Census__c(Id = mapNewCensusMembers.get(objCMId).vlocity_ins_CensusId__c, Primary_Member__c = ''));
        if(!lstCensusRemove.isEmpty())
            update lstCensusRemove;
    }

    /*
    @ method name       :   effectiveTermDates
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To add an error when the dates of the Census_Member__c object lie out of bounds of the Census__c object. 
    */

    //**********************************************************Task #6************************************************************ 
    public static void effectiveTermDates(List<Census_Member__c> lstNewCensusMembers){
        Set<Id> setCenId = new Set<Id>();
        for(Census_Member__c objCenMem: lstNewCensusMembers)
            setCenId.add(objCenMem.vlocity_ins_CensusId__c);
        Map<Id, Census__c> mapCensusEffTermDates = new Map<Id, Census__c>([SELECT Id, vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c FROM Census__c WHERE Id IN :setCenId]);
        Date dateCensusEffDate, dateCensusEndDate, dateCenMemEffDate, dateCenMemTermDate;
        
        for(Census_Member__c objCM: lstNewCensusMembers){
            if(!mapCensusEffTermDates.values().isEmpty()){
                //variables to make code more readable.
                dateCenMemEffDate = objCM.htcs_EffectiveDate__c;
                dateCenMemTermDate = objCM.htcs_TermDate__c;
                dateCensusEffDate = mapCensusEffTermDates.get(objCM.vlocity_ins_CensusId__c).vlocity_ins_EffectiveStartDate__c;
                dateCensusEndDate = mapCensusEffTermDates.get(objCM.vlocity_ins_CensusId__c).vlocity_ins_EffectiveEndDate__c;

                //Conditions to addError() if either/or effective and term dates are missing, either/or effective and term dates are outside bounds. 
                if((dateCenMemEffDate < dateCensusEffDate || dateCenMemTermDate > dateCensusEndDate))
                        objCM.addError('Member dates cannot be outside the bound of census!');
                }
            else 
                objCM.addError('Add Effective and Term dates in Census!');
        }
    }
    /*
    @ method name       :   singlePrimaryMemberInsert
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To add an error when the Primary_Member__c field is already populated and the isPrimary is set to TRUE for new insert. 
    */

    //*************************************************************Task #7 insert*******************************************************
    public static void singlePrimaryMemberInsert(List<Census_Member__c> lstNewCensusMembers){
        
        Set<Id> setCenId = new Set<Id>();
        for(Census_Member__c objCenMem: lstNewCensusMembers)
            setCenId.add(objCenMem.vlocity_ins_CensusId__c);
        Map<Id, Census__c> mapCenIdToCensus = new Map<Id, Census__c>([SELECT Id, Primary_Member__c FROM Census__c WHERE Id IN :setCenId]);
        
        for(Census_Member__c objCM: lstNewCensusMembers){
            //Condition to addError() when a second census member's checkbox is inserted as true.
            if(objCM.vlocity_ins_IsPrimaryMember__c == TRUE && mapCenIdToCensus.get(objCM.vlocity_ins_CensusId__c).Primary_Member__c != NULL)
                objCM.addError('Cannot have more than one Primary Member.');
        }
    }

    /*
    @ method name       :   singlePrimaryMemberUpdate
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To update the Primary_Member__c field of the Census__c with the name of the related Census_Member__c when updated. 
    */
    //*******************************************************************Task #7 update*************************************************
    public static void singlePrimaryMemberUpdate(Map<Id, Census_Member__c> mapNewCensusMembers, Map<Id, Census_Member__c> mapOldCensusMembers){
        
        Set<Id> setCenId = new Set<Id>();
        for(Census_Member__c objCenMem: mapNewCensusMembers.values())
            setCenId.add(objCenMem.vlocity_ins_CensusId__c);
        Map<Id, Census__c> mapCenIdToCensus = new Map<Id, Census__c>([SELECT Id, Primary_Member__c FROM Census__c WHERE Id IN :setCenId]);
        Boolean boolIsPrimaryMemNew, boolIsPrimaryMemOld;
        
        for(Id objNewMapId : mapNewCensusMembers.keySet()){
            boolIsPrimaryMemNew = mapNewCensusMembers.get(objNewMapId).vlocity_ins_IsPrimaryMember__c;
            boolIsPrimaryMemOld = mapOldCensusMembers.get(objNewMapId).vlocity_ins_IsPrimaryMember__c;
            //Condition to addError() when a second census member's checkbox is updated to true.
            if(mapCenIdToCensus.get(mapNewCensusMembers.get(objNewMapId).vlocity_ins_CensusId__c).Primary_Member__c != NULL
            && boolIsPrimaryMemNew != boolIsPrimaryMemOld
            && boolIsPrimaryMemNew == TRUE)
                mapNewCensusMembers.get(objNewMapId).addError('Cannot have more than one Primary Member.');
        }
    }

    /*
    @ method name       :   censusMembersInsert
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To update the Members__c field of the Census__c with the names of the related Census_Member__c separated by ';' when created. 
    */

    //***************************************************************Task #8 insert*****************************************************
    public static void censusMembersInsert(List<Census_Member__c> lstNewCensusMembers){
        
        Set<Id> setCenId = new Set<Id>();
        for(Census_Member__c objCenMem: lstNewCensusMembers)
            setCenId.add(objCenMem.vlocity_ins_CensusId__c);
        Map<Id, Census__c> mapCenIdToCensus = new Map<Id, Census__c>([SELECT Id, Members__c FROM Census__c WHERE Id IN :setCenId]);
        Id idCensusNew;
        
        for(Census_Member__c objCM: lstNewCensusMembers){
            idCensusNew = objCM.vlocity_ins_CensusId__c;
            //When Members__c is empty.
            if(mapCenIdToCensus.get(idCensusNew).Members__c == NULL)
                mapCenIdToCensus.put(idCensusNew, new Census__c(Id = idCensusNew, Members__c = objCM.Name));
            //When there is at least one member in Members__c, condition to handle semicolons.
            else
                mapCenIdToCensus.put(idCensusNew, new Census__c(Id = idCensusNew, Members__c = mapCenIdToCensus.get(idCensusNew).Members__c + ';' + objCM.Name));
        }
        
        if(!mapCenIdToCensus.values().isEmpty())
            update mapCenIdToCensus.values();
    }

    /*
    @ method name       :   censusMembersInsert
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To update the Members__c field of the Census__c with the names of the related Census_Member__c separated by ';' when updated. 
    */
    //****************************************************************Task #8 update*********************************************************
    public static void censusMembersUpdate(Map<Id, Census_Member__c> mapNewCensusMembers, Map<Id, Census_Member__c> mapOldCensusMembers){
        Set<Id> setCenId = new Set<Id>();
        for(Census_Member__c objCenMem: mapNewCensusMembers.values())
            setCenId.add(objCenMem.vlocity_ins_CensusId__c);
        Map<Id, Census__c> mapCenIdToCensus = new Map<Id, Census__c>([SELECT Id, Members__c FROM Census__c WHERE Id IN :setCenId]);
        Id idCensusNew; 
        String strCenMemNewName, strCenMemOldName, strMembers, strMembersFinal;
        for(Id objCMId: mapNewCensusMembers.keySet()){
            //variables to make code more readable.
            idCensusNew = mapNewCensusMembers.get(objCMId).vlocity_ins_CensusId__c;
            strCenMemNewName = mapNewCensusMembers.get(objCMId).Name;
            strCenMemOldName = mapOldCensusMembers.get(objCMId).Name;
            strMembers = mapCenIdToCensus.get(idCensusNew).Members__c;
            if(strCenMemNewName != strCenMemOldName && strMembers != NULL){
                strMembersFinal = strMembers.replace(strCenMemOldName, strCenMemNewName);
                mapCenIdToCensus.get(idCensusNew).Members__c = strMembersFinal;
            }
            else if(strMembers == NULL)
                mapCenIdToCensus.get(idCensusNew).Members__c = strCenMemNewName;
        }
        if(!mapCenIdToCensus.values().isEmpty())
        update mapCenIdToCensus.values();
    }
}

//**********************************************************Task #8 other update methods************************************************/

/*   public static void censusMembersUpdate(Map<Id, Census_Member__c> mapNewCensusMembers, Map<Id, Census_Member__c> mapOldCensusMembers){
        Set<Id> setCenId = new Set<Id>();
        for(Census_Member__c objCenMem: mapNewCensusMembers.values())
            setCenId.add(objCenMem.vlocity_ins_CensusId__c);
        Map<Id, Census__c> mapCenIdToCensus = new Map<Id, Census__c>([SELECT Id, Members__c FROM Census__c WHERE Id IN :setCenId]);
        Id idCensusNew; 
        String strCenMemNewName, strCenMemOldName;
        for(Id objCMId: mapNewCensusMembers.keySet()){
            //variables to make code more readable.
            idCensusNew = mapNewCensusMembers.get(objCMId).vlocity_ins_CensusId__c;
            strCenMemNewName = mapNewCensusMembers.get(objCMId).Name;
            strCenMemOldName = mapOldCensusMembers.get(objCMId).Name;

            if(strCenMemNewName != strCenMemOldName){

                //When Members__c is empty.
                if(mapCenIdToCensus.get(mapOldCensusMembers.get(objCMId).vlocity_ins_CensusId__c).Members__c == NULL)
                    mapCenIdToCensus.put(idCensusNew, new Census__c(Id = idCensusNew, Members__c =  strCenMemNewName));
                
                else{
                    //When updating the member at the beginning of Members__c.
                    if(mapCenIdToCensus.get(idCensusNew).Members__c.indexOf(strCenMemOldName) == 0){

                        //When there is only one member in the Members__c, condition to remove the semicolon.
                        if(mapCenIdToCensus.get(idCensusNew).Members__c.indexOf(';') == strCenMemOldName.length())
                            mapCenIdToCensus.put(idCensusNew, new Census__c(Id = idCensusNew, Members__c = mapCenIdToCensus.get(idCensusNew).Members__c.remove(strCenMemOldName + ';') + ';' + strCenMemNewName));
                        
                        //When there are 2 or members, condition to appropriately use semicolons.
                        else 
                            mapCenIdToCensus.put(idCensusNew, new Census__c(Id = idCensusNew, Members__c = mapCenIdToCensus.get(idCensusNew).Members__c.remove(strCenMemOldName) + strCenMemNewName));
                    }

                    //When updating members not in the beginning.
                    else 
                        mapCenIdToCensus.put(idCensusNew, new Census__c(Id = idCensusNew, Members__c = mapCenIdToCensus.get(idCensusNew).Members__c.remove(';' + strCenMemOldName) + ';' + strCenMemNewName));
                }
            }
        }

        if(!mapCenIdToCensus.values().isEmpty())
            update mapCenIdToCensus.values();
    }*/
    
/*
    public static void censusMembersUpdate(Map<Id, Census_Member__c> mapNewCensusMembers, Map<Id, Census_Member__c> mapOldCensusMembers){
        Boolean noChange = TRUE;
        Boolean isFirstNotNull = TRUE;
        Id idCensusNew;
        Set<Id> setCenId = new Set<Id>();
        for(Census_Member__c objCenMem: mapNewCensusMembers.values())
            setCenId.add(objCenMem.vlocity_ins_CensusId__c);

        Map<Id, Census__c> mapCenIdToCensus = new Map<Id, Census__c>([SELECT Id, Members__c FROM Census__c WHERE Id IN :setCenId]);

        for(Id idNewCM: mapNewCensusMembers.keySet())
            if(mapOldCensusMembers.get(idNewCM).Name != mapNewCensusMembers.get(idNewCM).Name)
                noChange = FALSE;

        if(noChange == FALSE){
            Map<Id, Census_Member__c> map_CMCenId_To_Census = new Map<Id, Census_Member__c>([SELECT Id, Name, vlocity_ins_CensusId__c FROM Census_Member__c WHERE vlocity_ins_CensusId__c IN :setCenId]);
            
            for(Census_Member__c objCM: map_CMCenId_To_Census.values()){
                idCensusNew = objCM.vlocity_ins_CensusId__c;
                //When Members__c is empty.
                if((mapCenIdToCensus.get(idCensusNew).Members__c != NULL && isFirstNotNull == TRUE) || mapCenIdToCensus.get(idCensusNew).Members__c == NULL){
                    mapCenIdToCensus.get(idCensusNew).Members__c = objCM.Name;
                    isFirstNotNull = FALSE;
                }
                else
                    mapCenIdToCensus.get(idCensusNew).Members__c += ';' + objCM.Name;
            }
            
            if(!mapCenIdToCensus.values().isEmpty())
                update mapCenIdToCensus.values();
        }
    }*/
