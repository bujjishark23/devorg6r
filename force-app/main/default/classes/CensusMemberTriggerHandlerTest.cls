/*
@ Class name        :   CensusMemberTriggerHandlerTest.cls
@ Created by        :   Rajasekhar Kallepalli
@ Created on        :   03-01-2022
@ Token/Jira Ticket :   SLIV - 70
@ Description       :   Create a CensusMemberTriggerHandlerTest Class which can be used to handle the unit tests for insurance app. 
*/

@isTest private class CensusMemberTriggerHandlerTest {
    @TestSetup
    static void makeData(){
        Account objAcc = new Account(Name = 'CenAcc');
        insert objAcc;

        Account objAccDate = new Account(Name = 'CenAccDate');
        insert objAccDate;
    }

    /*
    @ method name       :   CMTest
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To Test whether trigger updates the Primary_Member__c field of the Census__c with the name of the related Census_Member__c when created. 
    */

    @isTest public static void CMTest() {

        Account objAcc = [SELECT Name FROM Account WHERE Name = 'CenAcc'];

        //Update criteria for when the isPrimary is set to TRUE.
        Census__c objCenUp = new Census__c(Name = 'CenUp', vlocity_ins_GroupId__c = objAcc.Id);
        insert objCenUp;

        Census_Member__c objCenMem = new Census_Member__c(Name = 'CenMem', vlocity_ins_CensusId__c = objCenUp.Id, vlocity_ins_IsPrimaryMember__c = FALSE);
        insert objCenMem;
        
        Census_Member__c objCenMemUp = [SELECT Name, vlocity_ins_IsPrimaryMember__c FROM Census_Member__c WHERE Name = 'CenMem'];
        objCenMemUp.vlocity_ins_IsPrimaryMember__c = TRUE;
        update objCenMemUp;

        Census__c objCenQueriedUp = [SELECT Id, Name, Primary_Member__c FROM Census__c WHERE Name = 'CenUp'];
        System.assertEquals(objCenMemUp.Name, objCenQueriedUp.Primary_Member__c);
        
        //Update Criteria for when the isPrimary is set to FALSE.
        Census_Member__c objCenMemRem = [SELECT Name, vlocity_ins_IsPrimaryMember__c FROM Census_Member__c WHERE Name = 'CenMem'];
        objCenMemRem.vlocity_ins_IsPrimaryMember__c = FALSE;
        update objCenMemRem;

        Census__c objCenQueriedRem = [SELECT Id, Name, Primary_Member__c FROM Census__c WHERE Name = 'CenUp'];
        System.assertNotEquals(objCenMemUp.Name, objCenQueriedRem.Primary_Member__c);
    }

    /*
    @ method name       :   CMTestDates
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To test whether the trigger adds an error when the dates of the Census_Member__c object lie out of bounds of the Census__c object. 
    */

    @isTest public static void CMTestDates(){
        Account objAcc = [SELECT Name FROM Account WHERE Name = 'CenAccDate'];

        //Criteria for empty census dates in insert.
        Census__c objCen = new Census__c(Name = 'CenDate', vlocity_ins_GroupId__c = objAcc.Id, vlocity_ins_EffectiveStartDate__c = NULL, vlocity_ins_EffectiveEndDate__c = NULL);
        insert objCen;
        //Census dates empty, insert.
        try{
            Date objEffDate = Date.newInstance(2021, 12, 28);
            Date objTermDate = Date.newInstance(2022, 01, 05);
            Census_Member__c objCenMemIn = new Census_Member__c(Name = 'CenMemDate1', vlocity_ins_CensusId__c = NULL, htcs_EffectiveDate__c = objEffDate, htcs_TermDate__c = objTermDate); 
            insert objCenMemIn;
        }    

        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Add Effective and Term dates in Census!') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        //effective date out of bound. insert
        Date objEffDateCen = Date.newInstance(2021, 12, 25);
        Date objEndDateCen = Date.newInstance(2022, 01, 08);
        Census__c objCenWithEffEnd = [SELECT Name, vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c FROM Census__c WHERE Name = 'CenDate'];
        objCenWithEffEnd.vlocity_ins_EffectiveStartDate__c = objEffDateCen;
        objCenWithEffEnd.vlocity_ins_EffectiveEndDate__c = objEndDateCen;
        update objCenWithEffEnd;

        try{
            Date objEffDate = Date.newInstance(2021, 12, 23);
            Date objTermDate = Date.newInstance(2022, 01, 05);
            Census_Member__c objCenMemIn = new Census_Member__c(Name = 'CenMemDate1', vlocity_ins_CensusId__c = objCen.Id, htcs_EffectiveDate__c = objEffDate, htcs_TermDate__c = objTermDate); 
            insert objCenMemIn;
        } 
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Member dates cannot be outside the bound of census!') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        // term date out of bound. insert
        try{
            Date objEffDate = Date.newInstance(2021, 12, 27);
            Date objTermDate = Date.newInstance(2022, 01, 10);
            Census_Member__c objCenMemIn = new Census_Member__c(Name = 'CenMemDate1', vlocity_ins_CensusId__c = objCen.Id, htcs_EffectiveDate__c = objEffDate, htcs_TermDate__c = objTermDate); 
            insert objCenMemIn;
        } 
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Member dates cannot be outside the bound of census!') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        // both dates null.
        try{
            Census_Member__c objCenMemIn = new Census_Member__c(Name = 'CenMemDate1', vlocity_ins_CensusId__c = objCen.Id, htcs_EffectiveDate__c = NULL, htcs_TermDate__c = NULL); 
            insert objCenMemIn;
        } 
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Member dates cannot be outside the bound of census!') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        //both dates out of bound.
        Date objEffDate = Date.newInstance(2021, 12, 27);
        Date objTermDate = Date.newInstance(2022, 01, 04);
        Census_Member__c objCenMemUp = new Census_Member__c(Name = 'CenMemDate10', vlocity_ins_CensusId__c = objCen.Id, htcs_EffectiveDate__c = objEffDate, htcs_TermDate__c = objTermDate); 
        insert objCenMemUp;
        //effective date out of bound. update
        try{
            Census_Member__c objCenMemUpTest = [SELECT Name, htcs_EffectiveDate__c, htcs_TermDate__c FROM Census_Member__c WHERE Name = 'CenMemDate10'];
            Date objEffDateUp = Date.newInstance(2021, 12, 23);
            Date objTermDateUp = Date.newInstance(2022, 01, 06);
            objCenMemUpTest.htcs_EffectiveDate__c = objEffDateUp;
            objCenMemUpTest.htcs_TermDate__c = objTermDateUp;
            update objCenMemUpTest;
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Member dates cannot be outside the bound of census!') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        //term date out of bound. update
        try{
            Census_Member__c objCenMemUpTest = [SELECT Name, htcs_EffectiveDate__c, htcs_TermDate__c FROM Census_Member__c WHERE Name = 'CenMemDate10'];
            Date objEffDateUp = Date.newInstance(2021, 12, 26);
            Date objTermDateUp = Date.newInstance(2022, 01, 11);
            objCenMemUpTest.htcs_EffectiveDate__c = objEffDateUp;
            objCenMemUpTest.htcs_TermDate__c = objTermDateUp;
            update objCenMemUpTest;
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Member dates cannot be outside the bound of census!') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        // both dates updated to null.
        try{
            Census_Member__c objCenMemUpTest = [SELECT Name, htcs_EffectiveDate__c, htcs_TermDate__c FROM Census_Member__c WHERE Name = 'CenMemDate10'];
            objCenMemUpTest.htcs_EffectiveDate__c = NULL;
            objCenMemUpTest.htcs_TermDate__c = NULL;
            update objCenMemUpTest;
        } 
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Member dates cannot be outside the bound of census!') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        //census dates are empty. update
        Census__c objCenWithEffEndUp = [SELECT Name, vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c FROM Census__c WHERE Name = 'CenDate'];
        objCenWithEffEndUp.vlocity_ins_EffectiveStartDate__c = NULL;
        objCenWithEffEndUp.vlocity_ins_EffectiveEndDate__c = NULL;
        update objCenWithEffEndUp;

        try{
            Census_Member__c objCenMemUpTest = [SELECT Name, htcs_EffectiveDate__c, htcs_TermDate__c FROM Census_Member__c WHERE Name = 'CenMemDate10'];
            Date objEffDateUp = Date.newInstance(2021, 12, 30);
            Date objTermDateUp = Date.newInstance(2022, 01, 03);
            objCenMemUpTest.htcs_EffectiveDate__c = objEffDateUp;
            objCenMemUpTest.htcs_TermDate__c = objTermDateUp;
            update objCenMemUpTest;
        }    

        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Add Effective and Term dates in Census!') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }

    /*
    @ method name       :   singlePrimaryMemberInsertTest
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To Test whether trigger adds an error when the Primary_Member__c field is already populated and the isPrimary is set to TRUE for new insert. 
    */

    //Task #7 insert
    @isTest public static void singlePrimaryMemberInsertTest(){
        Account objAcc = [SELECT Name FROM Account WHERE Name = 'CenAcc'];

        Census__c objCenIn = new Census__c(Name = 'CenIn', vlocity_ins_GroupId__c = objAcc.Id);
        insert objCenIn;

        Census_Member__c objCenMemIn = new Census_Member__c(Name = 'CenMem1', vlocity_ins_CensusId__c = objCenIn.Id, vlocity_ins_IsPrimaryMember__c = TRUE);
        insert objCenMemIn;

        try{
            Census_Member__c objCenMemIn2 = new Census_Member__c(Name = 'CenMem2', vlocity_ins_CensusId__c = objCenIn.Id, vlocity_ins_IsPrimaryMember__c = TRUE);
            insert objCenMemIn2;
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot have more than one Primary Member.') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }

    /*
    @ method name       :   singlePrimaryMemberUpdateTest
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To test whether trigger updates the Primary_Member__c field of the Census__c with the name of the related Census_Member__c when updated. 
    */

    //Task #7 update
    @isTest public static void singlePrimaryMemberUpdateTest(){
        //Task #7 update
        Account objAcc = [SELECT Name FROM Account WHERE Name = 'CenAcc'];
        Census__c objCenUp = new Census__c(Name = 'CenUp', vlocity_ins_GroupId__c = objAcc.Id);
        insert objCenUp;

        Census_Member__c objCenMemIn = new Census_Member__c(Name = 'CenMem1', vlocity_ins_CensusId__c = objCenUp.Id, vlocity_ins_IsPrimaryMember__c = TRUE);
        insert objCenMemIn;
        try{
            Census_Member__c objCenMem2 = new Census_Member__c(Name = 'CenMem2', vlocity_ins_CensusId__c = objCenUp.Id, vlocity_ins_IsPrimaryMember__c = FALSE);
            insert objCenMem2;
            Census_Member__c objCenMemUp2 = [SELECT Name, vlocity_ins_IsPrimaryMember__c FROM Census_Member__c WHERE Name = 'CenMem2'];
            objCenMemUp2.vlocity_ins_IsPrimaryMember__c = TRUE;
            update objCenMemUp2;
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot have more than one Primary Member.') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }

    /*
    @ method name       :   censusMembersInsertTest
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To test whether trigger updates the Members__c field of the Census__c with the names of the related Census_Member__c separated by ';' when created. 
    */

    //Task #8 insert
    @isTest public static void censusMembersInsertTest(){
        Account objAcc = [SELECT Name FROM Account WHERE Name = 'CenAcc'];

        Census__c objCen8 = new Census__c(Name = 'CenUp8', vlocity_ins_GroupId__c = objAcc.Id, Members__c = NULL);
        insert objCen8;

        Census_Member__c objCenMem8 = new Census_Member__c(Name = 'CenMem8', vlocity_ins_CensusId__c = objCen8.Id);
        insert objCenMem8;
     
        Census__c objCen8Queried = [SELECT Members__c, Name FROM Census__c WHERE Name = 'CenUp8'];
        System.assertEquals('CenMem8', objCen8Queried.Members__c);
    }

    /*
    @ method name       :   censusMembersUpdateTest
    @ Created by        :   Rajasekhar Kallepalli
    @ Created on        :   03-01-2022
    @ Token/Jira Ticket :   SLIV - 70
    @ Description       :   To test whether trigger updates the Members__c field of the Census__c with the names of the related Census_Member__c separated by ';' when updated. 
    */

    //Task #8 update
    @isTest public static void censusMembersUpdateTest(){
        Account objAcc = [SELECT Name FROM Account WHERE Name = 'CenAcc'];

        Census__c objCen8 = new Census__c(Name = 'CenUp8', vlocity_ins_GroupId__c = objAcc.Id, Members__c = NULL);
        insert objCen8;

        Census_Member__c objCenMem8 = new Census_Member__c(Name = 'CenMem8', vlocity_ins_CensusId__c = objCen8.Id);
        insert objCenMem8;

        Census_Member__c objCenMem8Queried = [SELECT Name FROM Census_Member__c WHERE Name = 'CenMem8'];
        objCenMem8Queried.Name = 'CenMemUp8';
        update objCenMem8Queried;
        Census__c objCen8UpQueried = [SELECT Members__c, Name FROM Census__c WHERE Name = 'CenUp8'];
        System.assertEquals('CenMemUp8', objCen8UpQueried.Members__c);
        //=====
        objCen8UpQueried.Members__c = NULL;
        update objCen8UpQueried;
        Census_Member__c objCenMem8QueriedWhenMemIsNull = [SELECT Name FROM Census_Member__c WHERE Name = 'CenMemUp8'];
        objCenMem8QueriedWhenMemIsNull.Name = 'CenMemUp8Null';
        update objCenMem8QueriedWhenMemIsNull;
        Census__c objCen8UpQueriedWhenMemIsNull = [SELECT Members__c, Name FROM Census__c WHERE Name = 'CenUp8'];
        System.assertEquals('CenMemUp8Null', objCen8UpQueriedWhenMemIsNull.Members__c);
        //=====
        objCen8UpQueriedWhenMemIsNull.Members__c = 'CenMemUp8Null';
        update objCen8UpQueriedWhenMemIsNull; 
        Census_Member__c objCenMem8SecMem = new Census_Member__c(Name = 'CenMem8SecMem', vlocity_ins_CensusId__c = objCen8.Id);
        insert objCenMem8SecMem;
        Census_Member__c objCenMem8QueriedWithCol = [SELECT Name FROM Census_Member__c WHERE Name = 'CenMemUp8Null'];
        objCenMem8QueriedWithCol.Name = 'CenMemUp8WithCol';
        update objCenMem8QueriedWithCol;
        Census__c objCen8UpQueriedWithCol = [SELECT Members__c, Name FROM Census__c WHERE Name = 'CenUp8'];
        System.assertEquals('CenMemUp8WithCol;CenMem8SecMem', objCen8UpQueriedWithCol.Members__c);
        //=====
        Census_Member__c objCenMem8QueriedIndexNotAtStart = [SELECT Name FROM Census_Member__c WHERE Name = 'CenMemUp8WithCol'];
        objCenMem8QueriedIndexNotAtStart.Name = 'CenMemUp8IndexNotAtStart';
        update objCenMem8QueriedIndexNotAtStart;
        Census__c objCen8UpQueriedIndexNotAtStart = [SELECT Members__c, Name FROM Census__c WHERE Name = 'CenUp8'];
        System.assertEquals('CenMemUp8IndexNotAtStart;CenMem8SecMem', objCen8UpQueriedIndexNotAtStart.Members__c);
    }

}