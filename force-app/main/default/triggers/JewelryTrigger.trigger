trigger JewelryTrigger on Jewelry__c (before insert, before update) {
	if(Trigger.isBefore){
    	if(Trigger.isInsert)
			JewelryTriggerHandler.onBeforeInsert(Trigger.new);

    	if(Trigger.isUpdate)
			JewelryTriggerHandler.onBeforeUpdate(Trigger.newMap, Trigger.oldMap);
	}
}