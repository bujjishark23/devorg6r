trigger CensusMemberTrigger on Census_Member__c (before insert, before update, after insert, after update) {
    
    if(Trigger.isAfter){
    	if(Trigger.isInsert)
			CensusMemberTriggerHandler.onAfterInsert(Trigger.new);

    	if(Trigger.isUpdate)
			CensusMemberTriggerHandler.onAfterUpdate(Trigger.newMap, Trigger.oldMap);
	}
    
    if(Trigger.isBefore){
    	if(Trigger.isInsert)
			CensusMemberTriggerHandler.onBeforeInsert(Trigger.new);

    	if(Trigger.isUpdate)
			CensusMemberTriggerHandler.onBeforeUpdate(Trigger.newMap, Trigger.oldMap);
	}

}